# imports
from datetime import datetime
from pytz import timezone
from urllib.request import urlopen
from ics import Calendar, Event
import arrow
import yaml
import math

import utils
from utils_courses import Course

# variables
calendar_format_full = '%A %d %B %Hh%M'
calendar_format_full_seconds = '%A %d %B %Hh%Mm%S'
calendar_format_day = '%A %d %B'
calendar_format_hour = '%Hh%M'
calendar_timezone = timezone('Europe/Brussels')

# function : download and parse calendar to yaml
def download_and_parse_calendar(ucl2ics_id: int):
	# read calendar
	calendar = Calendar(urlopen('https://ucl2ics.appspot.com/get?key=' + str(ucl2ics_id)).read().decode())
	# read calendar
	config_calendar = {}
	courses = []
	config_id = 0
	for event in calendar.events:
		# dates
		start_date = datetime.strptime(event.begin.strftime('%d-%m-%Y %H:%M'), '%d-%m-%Y %H:%M')
		end_date = datetime.strptime(event.end.strftime('%d-%m-%Y %H:%M'), '%d-%m-%Y %H:%M')
		# course
		course_id = event.description.upper()
		# location
		locations = []
		location_words = event.location.split(' ') # examples of event location : CYCL 01 CYCL 02 CYCL 03, MONT 10, Fisher Candix DAO
		i = 0
		while i < len(location_words):
			word = location_words[i]
			# has next word
			i += 1
			if i < len(location_words):
				next = location_words[i]
				# number, linked to this word
				if utils.hasNumbers(next):
					locations.append(word + ' ' + next)
					i += 1
					continue
			# not linked to this or no next word
			locations.append(word)
		# add course
		config_id += 1
		config_id_str = make_config_id(config_id, 5)
		courses.append(Course(config_id_str, start_date, end_date, course_id, locations))
	# save file
	for course in courses:
		config_calendar[course.id] = course.to_yaml_dict()
	# save yaml
	yaml.dump(config_calendar, open('resources/calendar-' + str(ucl2ics_id) + '.yml', 'w+'))

def make_config_id(config_id: int, length: int) -> str:
	result = str(config_id)
	while (len(result) < length):
		result = '0' + result
	return 'course' + result
