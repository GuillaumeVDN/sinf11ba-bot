from datetime import datetime
import os

import utils_calendar

def log(line):
	# build line
	now = datetime.now().astimezone(utils_calendar.calendar_timezone)
	line = '[' + now.strftime(utils_calendar.calendar_format_full_seconds) + '] ' + line + '\n'
	# append line to file
	try:
		if os.path.exists('../data/logs.log'):
			with open('../data/logs.log', 'a') as file:
				file.write(line)
		else:
			with open('../data/logs.log', 'w+') as file:
				file.write(line)
	except:
		pass
