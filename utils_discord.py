# get channel by id
def get_role_by_id(guild, id: int):
	for role in guild.roles:
		if role.id == id:
			return role
	return None

def get_channel_by_id(guild, id: int):
	for channel in guild.channels:
		if channel.id == id:
			return channel
	return None
