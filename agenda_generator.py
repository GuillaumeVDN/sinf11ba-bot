from PIL import Image, ImageDraw, ImageFont, ImageShow
import yaml
import time
import datetime
import utils_calendar

LINE_THICKNESS = 2
HEAD_SIZE = 50
BOTTOM_SIZE = 20
MARGIN_SIZE = 50
FONT_SIZE_1 = 40
FONT_SIZE_2 = 20
FONT_SIZE_EVT = 15
FONT_1 = ImageFont.truetype("resources/font.otf", FONT_SIZE_1)
FONT_2 = ImageFont.truetype("resources/font.otf", FONT_SIZE_2)
FONT_EVT = ImageFont.truetype("resources/font.otf", FONT_SIZE_EVT)
WIDTH_DAY = 300
HOUR_HEIGHT = 70


def draw_header(draw, header, x_size):
	"""Draw the top header line and title"""
	head_text_size = draw.textsize(header, FONT_1)
	x_head_text = (x_size + MARGIN_SIZE - head_text_size[0]) / 2
	y_head_text = (HEAD_SIZE - head_text_size[1]) / 2
	draw.text((x_head_text, y_head_text), header, font=FONT_1, fill='black')
	draw.line([(0, HEAD_SIZE), (x_size, HEAD_SIZE)], 'black', LINE_THICKNESS)
	return draw


def draw_timetable_grid(draw, x_size, y_size, start_hour, end_hour):
	"""Draw the horizontal lines and time indication"""
	draw.line([(MARGIN_SIZE, 0), (MARGIN_SIZE, y_size)], 'black', LINE_THICKNESS)
	for k in range(end_hour-start_hour+2):
		draw.line([(MARGIN_SIZE, HEAD_SIZE + 70 + k * 70), (x_size, HEAD_SIZE + 70 + k * 70)], (128, 128, 128), LINE_THICKNESS)
		h_size = draw.textsize(str(start_hour + k) + "h00",FONT_2)
		if k != end_hour-start_hour+1:
			draw.text((5, HEAD_SIZE + 70 + k * 70 - h_size[1] / 2), (str(8 + k) + "h00"), font=FONT_2, fill='black')
	return draw


def draw_days_separator(draw, x_size, y_size, days):
	"""Draw vertical separatorsbetween days, and add the day name"""
	for k in range(len(days)):
		y = MARGIN_SIZE+(k+1)*WIDTH_DAY
		draw.line([(y, HEAD_SIZE),(y, y_size)], (128, 128, 128), LINE_THICKNESS)
		d_size = draw.textsize(days[k], font=FONT_2)
		draw.text(((y-(WIDTH_DAY+d_size[0])/2),HEAD_SIZE+(HOUR_HEIGHT-d_size[1])/2), days[k], font=FONT_2, fill='black')
	return draw


def draw_daily_layout(header: str):
	""""""
	x_size = WIDTH_DAY+MARGIN_SIZE
	y_size = HEAD_SIZE+HOUR_HEIGHT*12
	layout_day = Image.new('RGBA', (x_size, y_size), color='white')
	draw = ImageDraw.Draw(layout_day)
	draw = draw_header(draw, header, x_size)
	draw = draw_timetable_grid(draw, x_size, y_size,8,18)
	layout_day.show()


def draw_weekly_layout(header: str):
	x_size = WIDTH_DAY*5+MARGIN_SIZE
	y_size = HEAD_SIZE + HOUR_HEIGHT * 12
	layout_day = Image.new('RGBA', (x_size, y_size), color='white')
	draw = ImageDraw.Draw(layout_day)
	draw = draw_header(draw, header, x_size)
	draw = draw_timetable_grid(draw, x_size, y_size, 8, 18)
	draw = draw_days_separator(draw, x_size, y_size, ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'])
	layout_day.show()


def draw_evt(event):
	"""Draw a single event"""
	height=event[end_date]-event["start_date"]/3600*HOUR_HEIGHT
	img = Image.new('RGBA', (WIDTH_DAY,height), color='alpha')
	draw = ImageDraw.Draw(img)
	t_size = draw.textsize(event["course"], font=FONT_EVT)
	draw.text(((WIDTH_DAY-t_size[0])/2, (height-t_size[1])/2), event["course"], font=FONT_EVT, fill='black')
	return img


def draw_day_evts(date):
	"""Draw all events of a day"""
	img = Image.new('RGBA', (WIDTH_DAY, HOUR_HEIGHT*10), color='alpha')
	start, end = utils_calendar.daylimits_from_date(date)
	events = utils_calendar.retrieve_events(start, end)
	for evt in events:
		evt_img = draw_evt(evt)
		img.paste()


def draw_daily_agenda(calendar, date):
	
	print(test)
	print(datetime.datetime.fromtimestamp(test))
	utils_calendar.retrieve_events()

def draw_weekly_agenda():
	print("ok")

calendar = yaml.safe_load(open("resources/calendar.yml"))
draw_daily_layout("Test")
draw_weekly_layout("Test2")
#draw_daily_agenda(calendar[1])