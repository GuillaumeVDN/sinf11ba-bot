# imports
from datetime import datetime
import yaml
from threading import Timer

# timer
class RepeatTimer:

	def __init__(self, interval, runnable, *runnable_args):
		self.timer = None
		self.interval = interval
		self.runnable = runnable
		self.runnable_args = runnable_args
		self.is_running = False
		self.start()

	def run(self):
		self.is_running = False
		self.start()
		self.runnable(self.runnable_args)

	def start(self):
		if not self.is_running:
			self.timer = Timer(self.interval, self.run)
			self.timer.start()
			self.is_running = True

	def stop(self):
		self.timer.cancel()
		self.is_running = False

# locale
locale = None
def get_locale() -> dict:
	global locale
	if locale is None:
		with open('resources/locale.yml', encoding='utf8') as stream:
			locale = yaml.safe_load(stream)
		if locale is None:
			locale = {}
	return locale

def get_locale_message(path, *replacers) -> str:
	message = None
	locale = get_locale()
	for path_part in path.split('.'):
		message = locale[path_part] if message is None else message[path_part]
	for i in range(0, len(replacers), 2):
		placeholder = str(replacers[i])
		replacer = str(replacers[i + 1])
		message = message.replace(placeholder, replacer)
	return message

def get_locale_message_list(path, *replacers) -> list:
	message = None
	locale = get_locale()
	for path_part in path.split('.'):
		message = locale[path_part] if message is None else message[path_part]
	message = message.copy()
	for i in range(0, len(replacers), 2):
		placeholder = str(replacers[i])
		replacer = str(replacers[i + 1])
		for index in range(len(message)):
			message[index] = message[index].replace(placeholder, replacer)
	return message

# function : strings
def hasNumbers(inputString) -> bool:
	return any(char.isdigit() for char in inputString)

def hasChar(inputString, inputChar) -> bool:
	return any(char == inputChar for char in inputString)

def getPluralIfHasChar(string, char) -> str:
	result = string
	if (hasChar(string, char)):
		return 's'
	return ''

# function : arrays
def arrayToNiceString(array) -> str:
	result = ''
	length = len(array)
	for index in range(0, length):
		result += str(array[index])
		if index + 1 < length: result += ', '
	return result

# function : get daily interval from a specific date
def daylimits_from_date(date) -> datetime:
	start_date = date.replace(hour=0, minute=0, second=0, microsecond=1)
	end_date = date.replace(hour=23, minute=59, second=59, microsecond=1)
	return (start_date, end_date)
