# imports
import utils
import utils_calendar

import csv
import yaml
from datetime import datetime
import os

# Class config class
class ClassConfig:

	def __init__(self, id: int, ucl2ics_id: int, role_id: int, calendar_channel_id: int):
		self.id = id
		self.ucl2ics_id = ucl2ics_id
		self.role_id = role_id
		self.calendar_channel_id = calendar_channel_id

# function : get classes config
config_classes = None
def get_classes_config() -> dict:
	global config_classes
	if config_classes is None:
		config_classes = {}
		with open('resources/listing-classes.csv', encoding='utf8') as csv_file:
			csv_reader = csv.reader(csv_file, delimiter=',')
			for row in csv_reader:
				class_id = int(row[0])
				ucl2ics_id = int(row[1])
				role_id = int(row[2])
				calendar_channel_id = int(row[3])
				config_classes[class_id] = ClassConfig(class_id, ucl2ics_id, role_id, calendar_channel_id)
	return config_classes

def get_class_config(class_id: int) -> ClassConfig:
	config_classes = get_classes_config()
	return config_classes[class_id] if class_id in config_classes else None
