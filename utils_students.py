# imports
import csv
import os

import utils
import utils_calendar

# Student config class
class StudentConfig:

	def __init__(self, noma: str, last_name: str, first_name: str, student_class: int):
		self.noma = noma
		self.last_name = last_name
		self.first_name = first_name
		self.student_class = student_class

# function : get students config
config_students = None
def get_students_config() -> dict:
	global config_students
	if config_students is None:
		config_students = {}
		with open('resources/listing-students.csv', encoding='utf8') as csv_file:
			csv_reader = csv.reader(csv_file, delimiter=',')
			for row in csv_reader:
				noma = None if '?' in row[0] else row[0]
				last_name = row[1]
				first_name = row[2]
				student_class = None if row[3] == '?' or len(row[3]) == 0 else int(row[3])
				config_students[noma] = StudentConfig(noma, last_name, first_name, student_class)
	return config_students

def get_student_config(noma: str) -> dict:
	config_students = get_students_config()
	return config_students[noma] if noma in config_students else None
