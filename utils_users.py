# imports
import yaml
import os.path
import utils_students
import utils_classes
import utils_discord

# User data class
class UserData:

	def __init__(self, discord_id: str, noma: str):
		self.discord_id = discord_id
		self.noma = noma
	
	def get_student_config():
		return utils_students.get_student_config(noma)

# functions : get users data
data_users = None
def get_users_data() -> dict:
	global data_users
	if data_users is None:
		data_users = {}
		if os.path.exists('../data/users.yml'):
			with open('../data/users.yml') as stream:
				config = yaml.safe_load(stream)
				if config is not None:
					for discord_id in config:
						noma = str(config[discord_id]['noma'])
						data_users[discord_id] = UserData(discord_id, noma)
	return data_users

def get_user_data(discord_id: str) -> dict:
	data_users = get_users_data()
	return data_users[discord_id] if discord_id in data_users else None

# functions : create/modify user data
def save_user_data(user_data: UserData):
	data_users[user_data.discord_id] = user_data
	save_users_data()

def save_users_data():
	data_users = get_users_data()
	config = {}
	for user in data_users.values():
		user_config = {}
		user_config['noma'] = str(user.noma)
		config[user.discord_id] = user_config
	if len(config) != 0:
		with open('../data/users.yml', 'w+') as stream:
			yaml.dump(config, stream)

# functions : synchronize roles
async def synchronize_roles(guild, user):
	# get classes roles
	classes_roles_ids = utils_classes.get_classes_config().keys()
	# get data and role he must have
	user_data = get_user_data(user.id)
	student_config = utils_students.get_student_config(user_data.noma) if not user_data is None and not user_data.noma is None else None
	class_config = utils_classes.get_class_config(student_config.student_class) if not student_config is None and not student_config.student_class is None else None
	must_have_role_id = class_config.role_id if not class_config is None  and not class_config.role_id is None else None
	# check roles
	for user_role in user.roles.copy():
		# should have this role
		if user_role.id == must_have_role_id:
			must_have_role_id = None
		# shouldn't have this role
		elif user_role.id in classes_roles_ids:
			await user.remove_roles(user_role)
	# add must have role
	if not must_have_role_id is None:
		must_have_role = utils_discord.get_role_by_id(guild, must_have_role_id)
		if not must_have_role is None:
			await user.add_roles(must_have_role)
