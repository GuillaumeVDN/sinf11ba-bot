# imports
import utils

import csv
import yaml
from datetime import datetime
import os

# Course config class
class CourseConfig:

	def __init__(self, id: str, full_name: str, short_name: str, teacher: str):
		self.id = id
		self.full_name = full_name
		self.short_name = short_name
		self.teacher = teacher

# Course class
class Course:
	
	def __init__(self, id: str, start_date: datetime, end_date: datetime, course_id: str, locations: list):
		self.id = id
		self.start_date = start_date
		self.end_date = end_date
		self.course_id = course_id
		self.locations = locations

	def to_yaml_dict(self) -> dict:
		config_event = {}
		config_event['start_date'] = self.start_date.timestamp()
		config_event['end_date'] = self.end_date.timestamp()
		config_event['course_id'] = self.course_id
		config_event['locations'] = self.locations
		return config_event

# import calendar utils after defining classes
import utils_calendar

# function : get courses config
config_courses = None
def get_courses_config() -> dict:
	global config_courses
	if config_courses is None:
		config_courses = {}
		with open('resources/listing-courses.csv', encoding='utf8') as csv_file:
			csv_reader = csv.reader(csv_file, delimiter=',')
			for row in csv_reader:
				course_id = row[0]
				full_name = row[1]
				short_name = row[2]
				teacher = row[3]
				config_courses[course_id] = CourseConfig(course_id, full_name, short_name, teacher)
	return config_courses

def get_course_config(course_id: str) -> CourseConfig:
	# get courses config
	config_courses = get_courses_config()
	# find config for course
	for key in config_courses.keys():
		if course_id.startswith(key):
			return config_courses[key]
	# couldn't find any
	return None

# function : get courses
courses = {}
def get_courses(ucl2ics_id: int, force_update: bool = False) -> list:
	# must update ?
	global courses
	if not ucl2ics_id in courses or force_update:
		# create file if doesn't exist
		if not os.path.exists('resources/calendar-' + str(ucl2ics_id) + '.yml'):
			utils_calendar.download_and_parse_calendar(ucl2ics_id)
		# read calendar
		config_calendar = yaml.safe_load(open('resources/calendar-' + str(ucl2ics_id) + '.yml'))
		# parse events
		result = []
		for config_id in config_calendar:
			course_config = config_calendar[config_id]
			start_date = datetime.fromtimestamp(int(course_config['start_date'])).astimezone(utils_calendar.calendar_timezone)
			end_date = datetime.fromtimestamp(int(course_config['end_date'])).astimezone(utils_calendar.calendar_timezone)
			course_id = course_config['course_id']
			locations = course_config['locations']
			result.append(Course(config_id, start_date, end_date, course_id, locations))
		# sorts results by date
		result.sort(key=lambda r: r.start_date)
		courses[ucl2ics_id] = result
	# don't update
	else:
		result = courses[ucl2ics_id]
	# done
	return result

# function : retrieve courses in date range
def get_courses_in_range(ucl2ics_id: int, start_date: datetime, end_date: datetime, force_update: bool = False) -> list:
	# retrieve courses in range
	result = []
	for course in get_courses(ucl2ics_id, force_update):
		course_start = course.start_date
		if course_start >= start_date and course_start <= end_date:
			result.append(course)
	return result
