from datetime import datetime
import asyncio
import random

import utils_classes
import utils_discord
import utils_calendar
import utils_courses
import utils
import utils_log

def run(guild, force_display: bool = False, classes = None):
	# if it's 00:00 display calendar in channels
	now = datetime.now().astimezone(utils_calendar.calendar_timezone)
	utils_log.log('Checking if must display calendars, hour is ' + str(now.hour) + ', minute is ' + str(now.minute) + ', force_display is ' + str(force_display))
	if (now.hour == 0 and now.minute == 0) or force_display:
		# display schedule for all classes
		start_date, end_date = utils.daylimits_from_date(now)
		if classes is None:
			classes = utils_classes.get_classes_config().values()
		for classobj in classes:
			# find channel
			channel = utils_discord.get_channel_by_id(guild, classobj.calendar_channel_id) if not classobj.calendar_channel_id is None else None
			if not channel is None:
				# download and parse calendar and get courses
				utils_calendar.download_and_parse_calendar(classobj.ucl2ics_id)
				courses = utils_courses.get_courses_in_range(classobj.ucl2ics_id, start_date, end_date, True)
				# build message
				message = ''
				for course in courses:
					if len(message) > 0:
						message += '\n\n'
					# course id and name
					course_name = None
					course_id = course.course_id
					course_config = utils_courses.get_course_config(course.course_id)
					if not course_config is None and not course_config.full_name is None:
						course_name = course_config.full_name
					else:
						course_name = course_id
					# course type
					if '_' in course_id:
						course_type = 'TP'
					elif '-' in course_id:
						course_type = 'cours magistral'
					else:
						course_type = 'type inconnu, j\'espère que ça sera une pêche aux canards'
					# no config or unknown teacher
					back = False
					if course_config is None or '?' in course_config.teacher:
						for part in utils.get_locale_message_list('calendar_display.daily_message_element_noteacher' if course_config is None else 'calendar_display.daily_message_element_unknownteacher',
							'{start_time}', course.start_date.strftime(utils_calendar.calendar_format_hour),
							'{end_time}', course.end_date.strftime(utils_calendar.calendar_format_hour),
							'{course_name}', course_name,
							'{course_type}', course_type,
							'{course_id}', course_id,
							'{locations}', utils.arrayToNiceString(course.locations)):
							if not back: back = True
							else: message += '\n'
							message += part
					# has teacher
					else:
						for part in utils.get_locale_message_list('calendar_display.daily_message_element',
							'{start_time}', course.start_date.strftime(utils_calendar.calendar_format_hour),
							'{end_time}', course.end_date.strftime(utils_calendar.calendar_format_hour),
							'{course_name}', course_name,
							'{course_type}', course_type,
							'{course_id}', course_id,
							'{locations}', utils.arrayToNiceString(course.locations),
							'{teacher}', course_config.teacher):
							if not back: back = True
							else: message += '\n'
							message += part
				# send message
				if len(message) != 0:
					asyncio.create_task(remove_pinned_and_send(channel, random.choice(utils.get_locale_message_list('calendar_display.daily_message_header_random', '{class_group_role_id}', classobj.role_id, '{date}', start_date.strftime(utils_calendar.calendar_format_day))) + '\n\n' + message))

async def remove_pinned_and_send(channel, text):
	# remove previously pinned messages from the bot
	pinned = await channel.pins()
	for pin in pinned:
		if pin.author.bot:
			await pin.unpin()
	# send message and pin it
	message = await channel.send(text)
	await message.pin()
