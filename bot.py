# imports
import discord
from discord.ext import commands
import os
import sys
from dotenv import load_dotenv
import datetime
import random

import utils
import utils_discord
import utils_calendar
import utils_courses
import utils_students
import utils_classes
import utils_users

import timer_calendar

# init client with commands
client = commands.Bot(command_prefix='!')
sinf_server = None

# reload config command
@client.command(name='reloadconfig', pass_context=True, case_insensitive=True)
async def reloadconfig(context):
	# no permission
	has_perm = False
	for user_role in context.message.author.roles:
		if user_role.id == 623476267597168656:
			has_perm = True
			break
	if not has_perm:
		print('User ' + context.message.author.name + ' tried to use command !reloadconfig but hasn\'t permission')
		return
	# log
	print('User ' + context.message.author.name + ' used command !reloadconfig')
	# reset configs (will be loaded when needed)
	utils_classes.config_classes = None
	utils_courses.config_courses = None
	utils_students.config_students = None
	utils_users.data_users = None

# role sync command
@client.command(name='syncroles', pass_context=True, case_insensitive=True)
async def syncroles(context, user: discord.Member = None):
	# no permission
	has_perm = False
	for user_role in context.message.author.roles:
		if user_role.id == 623476267597168656:
			has_perm = True
			break
	if not has_perm:
		print('User ' + context.message.author.name + ' tried to use command !syncroles but hasn\'t permission')
		return
	# log
	print('User ' + context.message.author.name + ' used command !syncroles, user', user)
	# synchronize
	if user is None:
		for member in sinf_server.members:
			await utils_users.synchronize_roles(sinf_server, member)
	else:
		await utils_users.synchronize_roles(sinf_server, user)

# calendar test command
@client.command(name='showcalendar', pass_context=True, case_insensitive=True)
async def testcalendar(context, class_id: int = None):
	# no permission
	has_perm = False
	for user_role in context.message.author.roles:
		if user_role.id == 623476267597168656:
			has_perm = True
			break
	if not has_perm:
		print('User ' + context.message.author.name + ' tried to use command !showcalendar but hasn\'t permission')
		return
	# log
	print('User ' + context.message.author.name + ' used command !showcalendar, class', class_id)
	# show calendar
	classes = None
	if not class_id is None and class_id in utils_classes.get_classes_config():
		classes = [utils_classes.get_class_config(class_id)]
	timer_calendar.run(sinf_server, True, classes)

# noma confirmation command
awaiting_noma_confirm = {}
@client.command(name='noma', pass_context=True)
async def noma(context, noma: str):
	# log
	print('User ' + context.message.author.name + ' used command !noma, noma', noma)
	# already registered
	user_data = utils_users.get_user_data(context.message.author.id)
	if not user_data is None and not user_data.noma is None:
		await context.send(utils.get_locale_message('noma_confirmation.already_registered',
					'{user}', context.message.author.id,
					'{noma}', user_data.noma))
		return
	# unknown NOMA
	student_config = utils_students.get_student_config(noma)
	if student_config is None:
		await context.send(utils.get_locale_message('noma_confirmation.unknown_noma',
					'{user}', context.message.author.id))
		return
	# noma already registered for someone else
	for user_data in utils_users.get_users_data().values():
		if user_data.noma == noma:
			conflicting_user = client.get_user(user_data.discord_id)
			if not conflicting_user is None:
				await context.send(utils.get_locale_message('noma_confirmation.noma_already_registered',
					'{user}', context.message.author.id,
					'{conflicting_user}', conflicting_user.id))
				return
	# confirmation
	message = await context.send(utils.get_locale_message('noma_confirmation.confirm_message',
		'{user}', context.message.author.id,
		'{first_name}', student_config.first_name,
		'{last_name}', student_config.last_name,
		'{student_class}', student_config.student_class,
		'{random}', random.choice(utils.get_locale_message_list('noma_confirmation.confirm_message_random'))))
	emoji = random.choice(sinf_server.emojis)
	await message.add_reaction(emoji)
	awaiting_noma_confirm[context.message.author.id] = [context.message.author.id, noma, emoji.id]

# event : message reaction add
@client.event
async def on_reaction_add(reaction, user):
	# noma link confirmation
	awaiting = awaiting_noma_confirm[user.id] if user.id in awaiting_noma_confirm else None
	if not awaiting is None and user.id == awaiting[0] and reaction.emoji.id == awaiting[2]:
		awaiting_noma_confirm.pop(user.id)
		# create user data
		user_data = utils_users.get_user_data(user.id)
		if user_data is None:
			user_data = utils_users.UserData(user.id, awaiting[1])
		else:
			user_data.noma = awaiting[1]
		utils_users.save_user_data(user_data)
		# confirmed but no class
		student_config = utils_students.get_student_config(user_data.noma)
		if student_config.student_class is None:
			await reaction.message.channel.send(utils.get_locale_message('noma_confirmation.confirm_success_noclass', '{user}', user.id))
		# confirmed with class
		else:
			await reaction.message.channel.send(utils.get_locale_message('noma_confirmation.confirm_success',
				'{user}', user.id,
				'{first_name}', student_config.first_name,
				'{last_name}', student_config.last_name,
				'{student_class}', student_config.student_class))
			class_config = utils_classes.get_class_config(student_config.student_class)
			if not class_config is None:
				class_role = utils_discord.get_role_by_id(sinf_server, class_config.role_id)
				if not class_role is None:
					await user.add_roles(class_role)

# event : client ready
calendar_timer = None

@client.event
async def on_ready():
	# log connection
	print(f'{client.user} has connected to Discord !')
	# get sinf server instance
	global sinf_server
	for guild in client.guilds:
		if guild.id == 623121180815261697:
			sinf_server = guild
			break
	# start timers
	global calendar_timer
	calendar_timer = utils.RepeatTimer(60, timer_calendar.run, sinf_server)

# stop command
@client.command(name='stop', pass_context=True, case_insensitive=True)
async def stop(context):
	# no permission
	has_perm = False
	for user_role in context.message.author.roles:
		if user_role.id == 623476267597168656:
			has_perm = True
			break
	if not has_perm:
		print('User ' + context.message.author.name + ' tried to use command !stp but hasn\'t permission')
		return
	# log
	print('User ' + context.message.author.name + ' used command !stop')
	# stop timers
	global calendar_timer
	if not calendar_timer is None:
		calendar_timer.stop()
		calendar_timer = None
	await client.logout()
	sys.exit()

# get token and run client
load_dotenv()
token = os.getenv('BOT_TOKEN')
client.run(token)

# print events for today
#now = datetime.datetime.now().astimezone(utils_calendar.calendar_timezone)
#start_date, end_date = utils.daylimits_from_date(now)
#utils_courses.print_courses_in_range(start_date, end_date, False, ['SINF11BA4'])
